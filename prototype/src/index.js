import React from 'react';
import ReactDOM from 'react-dom';

import '../node_modules/semantic-ui-css/semantic.min.css';
import '../node_modules/react-date-picker/index.css'
import '../public/css/semanticCompanion.css';
import '../public/css/index.css';
import './components/Component.css';

import _ from 'lodash';
import AgendaModel from './models/AgendaModel';
import Agenda from './components/Agenda';

var model = new AgendaModel();
ReactDOM.render(
    <Agenda model={model} />,
    document.getElementById('root')
);
