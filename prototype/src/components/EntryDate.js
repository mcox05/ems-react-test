import React, { Component } from 'react';
import moment from 'moment';

import BookingItem from './BookingItem';

class EntryDate extends Component {
    render() {
    let day = moment(this.props.agenda.date, 'YYYY/MM/DD');
    let noSchedule = this.props.agenda.emptyScheduleDateSpan;
    let isToday = day.clone().startOf('day').diff(moment().startOf('day')) === 0;
    
    return (
        <div className="comp entry-date ui grid rhythm-12px">
            <a style={ { visibility: 'hidden' } } name={'date/' + (noSchedule || this.props.agenda.date)}></a> 
            <div className="row date-row">
                <div className="fifteen wide column">
                    { isToday && !noSchedule && <span style={ { 'marginRight': '12px' } }>TODAY</span> }
                    {noSchedule ? (
                        <span>{ noSchedule }</span>
                    ) : (
                        <span> { day.format('ddd MMM D YYYY') } </span>
                    )}       
                </div>
            </div>
            <div className="row">
                <div className="sixteen wide column">
                    { noSchedule && <span>You have no bookings for these dates.</span>}
                    <div className="comp list-view">
                        {this.props.agenda.items.map(function(item, index){
                            return <BookingItem key={item.id} item={item} />;
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default EntryDate;
