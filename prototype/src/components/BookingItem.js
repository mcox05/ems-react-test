import React, { Component } from 'react';
import moment from 'moment';

class BookingItem extends Component {
  render() {
    var start = moment(this.props.item.start);
    var end = moment(this.props.item.end);
    var timeSpan = moment.duration(end.diff(start));
    var hours = timeSpan.hours();
    var mins = timeSpan.minutes();
    var duration = hours ? (hours + 'h') : '';
    duration += mins ? (mins + 'm') : '';

    var now = moment();
    var meetingHasStarted = now.isAfter(start) && now.isBefore(end);

    return (
        <div className="comp booking-item ui grid rhythm-2px">
            <div className="row">
                <div className="align-right four wide mobile two wide tablet two wide computer column">
                    { start.format('LT') }
                </div>
                <div className="ten wide mobile thirteen wide tablet thirteen wide computer column">
                    { this.props.item.eventName }
                </div>
                <div className="two wide mobile one wide tablet one wide computer column">
                    &nbsp;
                </div>
            </div>
            <div className="row">
                <div className="align-right four wide mobile two wide tablet two wide computer column">
                    { end.format('LT') }
                </div>
                <div className="ten wide mobile thirteen wide tablet thirteen wide computer column">
                    { this.props.item.roomName }
                </div>
                <div className="two wide mobile one wide tablet one wide computer column">
                    &nbsp;
                </div>
            </div>
            <div className="row">
                <div className="align-right four wide mobile two wide tablet two wide computer column">
                    { duration }
                </div>
                <div className="right floated two wide mobile one wide tablet one wide computer column">
                    { meetingHasStarted && <i className="checked calendar icon"></i>}
                </div>
            </div>
        </div>
    );
  }
}

export default BookingItem;

