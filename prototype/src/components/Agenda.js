import React, { Component } from 'react';
import moment from 'moment';

import MenuComp from './Menu';
import EntryDate from './EntryDate';

class Agenda extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: this.props.model.loadCalendar(),
            selectedDate: null
        };

        this.showNow = this.showNow.bind(this);
        this.handleSearchComplete = this.handleSearchComplete.bind(this);
        this.handleDatePicked = this.handleDatePicked.bind(this);
        this.handleBookingAdded = this.handleBookingAdded.bind(this);
        this.cancelSearch = this.cancelSearch.bind(this);
    }
    componentDidMount() {
        this.componentDidUpdate();
    }
    componentDidUpdate() {
        var header = document.querySelectorAll('.fixed .header')[0];
        var body = document.querySelectorAll('.fixed .body')[0];
        var footer = document.querySelectorAll('.fixed .footer')[0];
        body.style.top = header.clientHeight - 1 + 'px';
        body.style.bottom = footer.clientHeight + 'px';
    }
    cancelSearch() {
        this.setState({ results: this.props.model.cancelSearch() });
    }
    showNow(e) {
        this.refs.menu.onSearchCancelled(); //calls menu to update its state, which calls this.cancelSearch()
        this.props.model.filters.clear();
        var now = moment();
        this.refs.menu.onDateSelected(now.format('YYYY/MM/DD')); //calls menu to update state, which calls this.handleDatePicked()
    }
    handleSearchComplete(result) {
        this.setState({ results: this.props.model.search(result.value) });
    }
    handleDatePicked(event, date) {
        this.props.model.filters.date = date;
        this.setState({ results: this.props.model.loadCalendar() });
    }
    handleBookingAdded(booking) {
        this.props.model.filters.date = moment(booking.start, 'MM/DD/YYYY').format('YYYY/MM/DD hh:mm');
        this.props.model.addBooking(booking);
        this.setState({ results: this.props.model.loadCalendar() });
    }
    render() {
        let { results } = this.state;
        return (
            <div className="fixed">
                <div className="header">
                    <MenuComp ref="menu" 
                        onDayClick={ this.handleDatePicked } 
                        onBookingAdded={ this.handleBookingAdded } 
                        onSearchComplete={this.handleSearchComplete} 
                        onSearchCancelled={this.cancelSearch} />
                </div>
                <div className="body">
                    <div className="ui grid rhythm-12px">
                        <div className="sixteen wide column">
                        {results.length ? (
                              results.map((item, index) => {
                                return <EntryDate key={ index } agenda={ item } />;
                            })
                        ) : (
                            <h2 className="align-center">No schedule items found</h2>
                        )}
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <a onClick={ this.showNow } className="button now">Now</a>
                </div>
            </div>
        );
    }
}

export default Agenda;