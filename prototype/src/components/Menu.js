﻿import React, { Component } from 'react';
import { Modal, Button } from 'semantic-ui-react';
import { MonthView } from 'react-date-picker'

import moment from 'moment';
import Search from './Search';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            titleDate: moment().format('MMMM YYYY'),
            calendarIsOpen: false,
            addModalIsOpen: false,
            showSearch: false
        };

        this.onSearchCancelled = () => {
            this.setState({ showSearch: false });
            this.props.onSearchCancelled();
        };
        //search toggle
        this.searchToggle = () => {
            this.setState({ showSearch: !this.state.showSearch });
        };

        //modal callbacks
        this.open = () => this.setState({ addModalIsOpen: true });
        this.close = () => this.setState({ addModalIsOpen: false });

        this.handleEventName = (e) => this.setState({ eventName: e.target.value });
        this.handleRoomName = (e) => this.setState({ roomName: e.target.value });
        this.handleStart = (e) => this.setState({ start: e.target.value });
        this.handleEnd = (e) => this.setState({ end: e.target.value });
        this.isBookingValid = () => this.state.eventName && 
                                    this.state.roomName && 
                                    this.state.start && 
                                    this.state.end && 
                                    moment(this.state.start).isValid() 
                                    && moment(this.state.end).isValid();

        this.onDateSelected = this.onDateSelected.bind(this);
        this.toggleCalendar = this.toggleCalendar.bind(this);
        this.saveBooking = this.saveBooking.bind(this);
    }
    saveBooking() {
        this.close();
        var booking = {
            eventName: this.state.eventName,
            roomName: this.state.roomName,
            start: this.state.start,
            end: this.state.end
        };
        this.props.onBookingAdded(booking);
    }
    onDateSelected(dateString) {
        this.setState({
            selectedDate: dateString,
            titleDate: moment(dateString, 'YYYY/MM/DD hh:mm').format('MMMM YYYY'), 
            calendarIsOpen: false
        });
        this.props.onDayClick(event, dateString);
    }
    toggleCalendar() {
        this.setState({ calendarIsOpen: !this.state.calendarIsOpen });
    }
    componentDidMount() {
        this.componentDidUpdate();
    }
    componentDidUpdate() {
        let sticky = document.querySelectorAll('.fixed .header')[0];
        let body = document.querySelectorAll('.fixed .body')[0];
        body.style.top = sticky.clientHeight + 'px';
        let calendar = document.querySelectorAll('.react-flex.react-date-picker__month-view');
        if (calendar.length) { //center calendar
            calendar[0].style.marginLeft = ((body.clientWidth - calendar[0].clientWidth - 24) / 2) + 'px'; //24px additional to account for semantic gutter padding 
        }
    }
    render() {
        const { selectedDate, eventName, roomName, start, end } = this.state;
        return (
            <div className="comp my-menu ui grid rhythm-12px">
                <div className="row date-row">
                    <div className="sixteen wide column">
                        <div className="ui middle aligned grid">
                            <div className="row">
                                <div className="two wide mobile two wide tablet two wide computer column">
                                    <i className="content icon"></i>
                                </div>
                                <div className="ten wide mobile ten wide tablet ten wide computer column">
                                    <h3 className="month-year-display" onClick={ this.toggleCalendar }>
                                        { this.state.titleDate } 
                                        <i  className={"angle " + (this.state.calendarIsOpen ? 'up' : 'down') + " icon"}></i>
                                    </h3>
                                </div>                          
                                <div className="right aligned two wide mobile two wide tablet two wide computer column">
                                    <i className="search icon" onClick={this.searchToggle}></i>
                                </div>
                                <div className="right aligned two wide mobile two wide tablet two wide computer column">
                                    <Modal trigger={<i className="plus icon" onClick={ this.addBooking }></i>} closeIcon='close' open={this.state.addModalIsOpen} onOpen={this.open} onClose={this.close}>
                                        <Modal.Header>Schedule Booking</Modal.Header>
                                        <Modal.Content>
                                            <form className="ui form">
                                              <div className={"field " + (!eventName ? "error" : "")}>
                                                <label>Booking Name</label>
                                                <input type="text" onChange={this.handleEventName} />
                                              </div>
                                              <div className={"field " + (!roomName ? "error" : "")}>
                                                <label>Booking Room</label>
                                                <input type="text" onChange={this.handleRoomName} />
                                              </div>
                                              <div className={"field " + (!start || !moment(start).isValid() ? "error" : "")}>
                                                <label>Date & Start Time</label>
                                                <input type="text"  onChange={this.handleStart} placeholder="mm/dd/yyyy hh:mm:ss" />
                                              </div>
                                              <div className={"field " + (!end || !moment(end).isValid() ? "error" : "")}>
                                                <label>Date & End Time</label>
                                                <input type="text"  onChange={this.handleEnd} placeholder="mm/dd/yyyy hh:mm:ss" />
                                              </div>
                                            </form>
                                            <div className="ui divider"></div>
                                            {!this.isBookingValid() &&
                                            <div className="ui error message">
                                                <ul className="list">
                                                    {!eventName && <li>Booking Name is required</li>}
                                                    {!roomName && <li>Booking Room is required</li>}
                                                    {!start && <li>Date & Start Time are required</li>}
                                                    {!moment(start).isValid() && <li>Date & Start Time are in an invalid format</li>}
                                                    {!end && <li>Date & End Time are required</li>}
                                                    {!moment(end).isValid() && <li>Date & Start End are in an invalid format</li>}
                                                </ul>
                                            </div>
                                            }
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <Button negative icon='remove' labelPosition='left' content="Cancel" onClick={this.close} />
                                            <Button positive disabled={!this.isBookingValid()} icon='checkmark' labelPosition='left' content="Schedule" onClick={this.saveBooking}  />
                                        </Modal.Actions>
                                    </Modal>
                                </div>
                            </div>
                            {this.state.calendarIsOpen && 
                            <div className="row">
                                <div className="sixteen wide column">
                                     <MonthView style={ {marginLeft: '12px'} }
                                        dateFormat="YYYY/MM/DD hh:mm"
                                        date={selectedDate}
                                        onChange={this.onDateSelected}
                                      />
                                </div>
                            </div>
                            }
                            {this.state.showSearch && 
                            <div className="row">
                                <div className="sixteen wide column">
                                    <Search onSearchComplete={this.props.onSearchComplete} onCancel={this.onSearchCancelled} />
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu;