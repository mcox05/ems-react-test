﻿import React, { Component } from 'react';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: null
        };
    
        this.cancel = () => {
            this.setState({ filterText: null });
            this.props.onCancel();
        };

        this.search = () => {
            this.setState({ isLoading: true });
            this.props.onSearchComplete({ value: this.state.filterText });
            this.setState({ isLoading: false });
        };
        
        let throttleId = null;
        this.handleKeyPress = (e) => {
            if (!throttleId) { //add some crude throttling for performance
                throttleId = setTimeout(() => {
                    this.search();
                    throttleId = null;
                }, 300);
            }
        };
        this.handleOnChange = (e) => this.setState({ filterText: e.target.value });
    }
    componentDidMount() {
        document.querySelectorAll('.ui.fluid.input input[type="text"]')[0].focus();
    }
    render() {
    return (
            <div className={"ui " + (this.state.isLoading ? 'loading' : '') + " comp search"}>
                <div className="ui grid no-rhythm">
                    <div className="middle aligned row">
                        <div className="twelve wide mobile fourteen wide tablet fourteen wide computer column">
                            <div className="ui fluid icon input">
                                <i className="search icon"></i>
                                <input type="text" placeholder="Search..." onKeyPress={this.handleKeyPress} onChange={this.handleOnChange}/>
                            </div>
                        </div>
                        <div className="center aligned four wide mobile two wide tablet two wide computer column">
                            <a onClick={this.cancel}>Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu;