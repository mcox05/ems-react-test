﻿import _ from 'lodash';
import moment from 'moment';
import data from '../../public/bookings.json';


let todayItem = _.find(data.bookings, { id: 12 });
todayItem.start = moment();
todayItem.end = moment().add(1, 'hour');

class AgendaModel {
    constructor() {
        this.agenda = {};
        this.maxBookingId = 0;
        this.results = [];
        this.backup = null; //used to restore after canceling a search
        this.isSearching = false;
        
        let self = this;
        this.filters = {
            showGapDates: true,
            date: null,
            text: null,
            clear() {
                var cleared = this.now || this.text || this.date;
                this.showGapDates = true;
                this.text = null;
                this.date = null;
                return cleared;
            },
            restore() {
                if (self.backup) {
                    this.showGapDates = self.backup.showGapDates;
                    this.text = self.backup.text;
                    this.date = self.backup.date;
                }
            }
        };
        
        this.enumerateDaysBetweenDates = this.enumerateDaysBetweenDates.bind(this);
        this.getSortedKeys = this.getSortedKeys.bind(this);
        this.addBooking = this.addBooking.bind(this);
        this.getFilteredKeys = this.getFilteredKeys.bind(this);
        this.search = this.search.bind(this);
        this.cancelSearch = this.cancelSearch.bind(this);

        //loop over all entries and store the booking into a dictionary that calculates the unique days the item spans across. It uses the date(s) of the booking as keys
        data.bookings.forEach(item => {
            this.addBooking(item);
            if (this.maxBookingId < item.id) {
                this.maxBookingId = item.id;
            }
        });
    }
    //gets a list of all the dates between two dates (exclusive)
    enumerateDaysBetweenDates(startDate, endDate, inclusive=true) {
        let dates = [];
    
        let currDate = startDate.clone().startOf('day');
        let lastDate = endDate.clone().startOf('day');

        while (currDate.add(1, 'days') && ((inclusive && currDate <= lastDate) || (!inclusive && currDate < lastDate))) {
            dates.push(currDate.clone().format('YYYY/MM/DD'));
        }
    
        return dates;
    }
    cancelSearch() {
        if (this.isSearching) {  //reload the backup if they did a search
            this.filters.restore();
            this.isSearching = false;
            this.loadCalendar();
        }
        return this.results;
    }
    search(filterText) {
        this.filters.text = filterText;
        this.filters.showGapDates = false;
        this.isSearching = true;
        return this.loadCalendar();
    }
    //get all agenda lookup gets in ascending order
    getSortedKeys() {
        return Object.keys(this.agenda).sort();
    }
    getFilteredKeys() {
        var keys = Object.keys(this.agenda);
        var filteredKeys = keys;

        if (this.filters.text) {
            filteredKeys = keys.filter(dateKey => this.agenda[dateKey].items.some(item => {
                return item.eventName.toLowerCase().indexOf(this.filters.text) > -1 ||
                    item.roomName.toLowerCase().indexOf(this.filters.text) > -1;
            }));
        } else {
            var date = this.filters.date ? moment(this.filters.date, 'YYYY/MM/DD') : moment();
            var filterDate = date.startOf('day');
            filteredKeys = keys.filter(dateKey => {
                var dt = moment(dateKey, 'YYYY/MM/DD').startOf('day');
                return dt >= filterDate;
            });
        }

        return filteredKeys.sort();
    }
    //computes list of all agenda entries, also enforces filters, and can include empty schedule periods
    loadCalendar() {
        let keys = this.getFilteredKeys(),
            items = keys.map(key => this.agenda[key]),
            alteredItemsIndex = 0; 

        //identify gaps between booking dates and fill in with empty schedule dates span entries
        let handleEmptyDateSpans = function(endOfDateA, startOfDateB) {
            const days = this.enumerateDaysBetweenDates(moment(endOfDateA.maxEndDate, 'YYYY/MM/DD'), moment(startOfDateB.date, 'YYYY/MM/DD'), false);
            if (days.length) {
                let nothingScheduledBeginDate = moment(days[0], 'YYYY/MM/DD').startOf();
                let nothingScheduledEndDate = moment(days[days.length - 1], 'YYYY/MM/DD').startOf();
                let fakeObj = {
                     date: null, 
                     items: [], 
                     emptyScheduleDateSpan: nothingScheduledBeginDate.format('ddd MMM D') + ' - ' + nothingScheduledEndDate.format('ddd MMM D YYYY')
                };
                
                /* we need to make sure to alteredItemsIndex first because as items are inserted at this point, 
                   the keys array indices will not longer be in lock step with theitems array indices
                   it will be off by one more each time something is inserted thus the +1 
                */
                ++alteredItemsIndex;
                items.splice(alteredItemsIndex, 0, fakeObj); 
            }
        };

        let
            endOfDateA,
            startOfDateB;

        if (keys.length > 1 && this.filters.showGapDates) {
            //iterate of the keys, identify if there is a date gap, then insert a fake date span entry between.

            keys.forEach((key, i) => {
                endOfDateA = this.agenda[key];
                startOfDateB = this.agenda[keys[i + 1]];
                if (i < keys.length - 1) { //don't examine the last index. We don't put empty date spans after the last entry
                    handleEmptyDateSpans.call(this, endOfDateA, startOfDateB);
                }
                alteredItemsIndex++; //keep in lock step with i variable (assuming no items are inserted which will bump the increment up)
            });
        }
        
        this.results.length = 0; //don't lose the memory reference.
        Array.prototype.push.apply(this.results, items); //merge in items 

        if (!this.backup || !this.isSearching) {
            this.backup = Object.assign({}, this.filters);
        }
        this.filters.clear();
        return this.results;
    }

    addBooking(booking) {
        let dates = this.enumerateDaysBetweenDates(moment(booking.start), moment(booking.end)).concat([moment(booking.start).format('YYYY/MM/DD')]);

        dates.forEach(dateKey => {
            this.agenda[dateKey] = this.agenda[dateKey] || { date: dateKey, maxEndDate: dateKey, emptyScheduleDateSpan: null, items: [] }; //ensure init
            let agendaDay = this.agenda[dateKey];

            //keep track of max end date to make life easier later when we determine gaps between days
            let maxEndDate = agendaDay.maxEndDate;
            if (moment(dateKey, 'YYYY/MM/DD') > moment(maxEndDate, 'YYYY/MM/DD')) {
                agendaDay.maxEndDate = dateKey;
            }

            let bookingStartDate = moment(booking.start);
            let arrayLen = agendaDay.items.length;

            if (!booking.id) { //increment max id and assing if need be
                booking.id = this.maxBookingId++;
            }

            if (!agendaDay.items.length) {
                agendaDay.items.push(booking);
            }
            else {
                //perform insert at appropriate position chronologically
                var firstItem = moment(agendaDay.items[0].start);
                var lastItem = moment(agendaDay.items[arrayLen - 1].start);

                if (firstItem >= bookingStartDate) {
                    agendaDay.items.splice(0, 0, booking);
                } 
                else if (lastItem <= bookingStartDate) {
                    agendaDay.items.push(booking);
                }
                else if (arrayLen > 1) { //two or more items, find insert location
                    for (let i = 0; i < arrayLen - 1; i++) {
                        var before = moment(agendaDay.items[i].start);
                        var after = moment(agendaDay.items[i + 1].start);
                        if (bookingStartDate >= before && bookingStartDate <= after) {
                            agendaDay.items.splice(i + 1, 0, booking);
                        }
                    }    
                }
            }
        });
    }
}

export default AgendaModel;